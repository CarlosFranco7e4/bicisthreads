import java.time.Duration;
import java.time.LocalTime;

public class BiciLambda {
    public static void main(String[] args) {
        Runnable bici1 = () -> {
            LocalTime inici = LocalTime.now();
            int distancia = 1000;
            String nom = "Montse";
            int comptador = 0;
            while (comptador < distancia){
                comptador++;
            }
            long temps = Duration.between(inici,LocalTime.now()).getNano();
            System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
        };
        Thread t1 = new Thread(bici1);

        Thread bici2 = new Thread(() -> {
            LocalTime inici = LocalTime.now();
            int distancia = 1000;
            String nom = "Fran";
            int comptador = 0;
            while (comptador < distancia){
                comptador++;
            }
            long temps = Duration.between(inici,LocalTime.now()).getNano();
            System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
        });

        Thread bici3 = new Thread(() -> {
            LocalTime inici = LocalTime.now();
            int distancia = 1000;
            String nom = "Carla";
            int comptador = 0;
            while (comptador < distancia){
                comptador++;
            }
            long temps = Duration.between(inici,LocalTime.now()).getNano();
            System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
        });

        t1.start();
        bici2.start();
        bici3.start();
    }
}

import java.time.Duration;
import java.time.LocalTime;

public class BiciClasseAnonima {

    public static void main(String[] args) throws InterruptedException {
        Thread bici1 =new Thread( new Runnable() {
            private final LocalTime inici = LocalTime.now();
            private int comptador = 0;

            @Override
            public void run() {
                int distancia = 1000;
                while (comptador < distancia){
                    comptador++;

                }
                long temps = Duration.between(inici, LocalTime.now()).getNano();
                String nom = "Montse";
                System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
            }
        });
        bici1.start();

        Runnable bici2 = new Runnable(){
            private final LocalTime inici = LocalTime.now();
            private int comptador = 0;

            @Override
            public void run() {
                int distancia = 1000;
                while (comptador < distancia){
                    comptador++;
                }
                long temps = Duration.between(inici, LocalTime.now()).getNano();
                String nom = "Fran";
                System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
            }

        };

        Runnable bici3 = new Runnable(){
            private final LocalTime inici = LocalTime.now();
            private int comptador = 0;

            @Override
            public void run() {
                int distancia = 1000;
                while (comptador < distancia){
                    comptador++;
                }
                long temps = Duration.between(inici, LocalTime.now()).getNano();
                String nom = "Clara";
                System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
            }
        };
        Thread b2=new Thread(bici2);
        Thread b3=new Thread(bici3);
        b2.start();
        b3.start();
    }
}

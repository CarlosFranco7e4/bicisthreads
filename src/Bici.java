import java.time.Duration;
import java.time.LocalTime;

// La meva resposta no estava pujada i estava incompleta, la pujo ara amb els "retoques"

public class Bici extends Thread implements Comparable<Bici>{
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;
    private int comptador;

    public Bici(String nom, LocalTime inici, int distancia) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalTime getInici() {
        return inici;
    }

    public void setInici(LocalTime inici) {
        this.inici = inici;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    @Override
    public void run() {
        while (comptador < distancia){
            comptador++;
            // System.out.println();
        }
        temps = Duration.between(inici,LocalTime.now()).getNano();
        System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
    }

    @Override
    public int compareTo(Bici bici) {
        Bici b=(Bici)bici;
        return Long.compare(temps,b.temps);
    }
}

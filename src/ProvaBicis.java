import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;

public class ProvaBicis {

    public static void main(String[] args) {
        LocalTime inici = LocalTime.now();
        final int distancia = 1000;
        Bici b1 = new Bici("Montse", inici, distancia);
        Bici b2 = new Bici("Fran", inici, distancia);
        Bici b3 = new Bici("Clara", inici, distancia);

        ArrayList<Bici> bicis = new ArrayList<Bici>();
        bicis.add(b1);
        bicis.add(b2);
        bicis.add(b3);

        for (Bici bici : bicis){
            bici.start();
        }

        for (Bici bici : bicis){
            try {
                bici.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(bicis);
        String guanyador = bicis.get(0).getNom();

        System.out.println("\nLa bici més ràpìda és la de " + guanyador);
    }
}

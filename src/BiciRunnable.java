import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;


public class BiciRunnable implements Runnable {
    private String nom;
    private LocalTime inici;
    private int distancia;
    private long temps;
    private int comptador;

    public BiciRunnable(String nom, int distancia, LocalTime inici) {
        this.nom = nom;
        this.inici = inici;
        this.distancia = distancia;
    }

    @Override
    public void run() {
        while (comptador < distancia)
            comptador++;
        temps = Duration.between(inici,LocalTime.now()).getNano();
        System.out.println(nom + " ha trigat " + temps + " unitats de temps en fer una distancia de " + distancia);
    }

    public static void main(String[] args) throws InterruptedException {
        LocalTime inici = LocalTime.now();
        final int DISTANCIA = 1000;

        ArrayList<Thread> ArrayList = new ArrayList(Arrays.asList(new Thread(new BiciRunnable("Montse", DISTANCIA, inici)), new Thread(new BiciRunnable("Fran", DISTANCIA, inici)), new Thread(new BiciRunnable("Clara", DISTANCIA, inici))));

        for (Thread bici : ArrayList)
            bici.start();

        for (Thread bici : ArrayList)
            bici.join();
    }
}